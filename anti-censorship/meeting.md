Anti-censorship
--------------------------------

Next meeting: Thursday, Dec 21 16:00 UTC
Facilitator: meskio

Weekly meetings, every Thursday at 16:00 UTC, in #tor-meeting at OFTC
(channel is logged while meetings are in progress)

This week's Facilitator: onyinyang


== Goal of this meeting ==

Weekly check-in about the status of anti-censorship work at Tor.
Coordinate collaboration between people/teams on anti-censorship at the Tor Project and Tor community.


== Links to Useful documents ==
	* Our anti-censorship roadmap:
		* Roadmap:https://gitlab.torproject.org/groups/tpo/anti-censorship/-/boards
	* The anti-censorship team's wiki page:
		* https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/home
	* Past meeting notes can be found at:
		* https://lists.torproject.org/pipermail/tor-project/
	* Tickets that need reviews: from sponsors, we are working on:
		* All needs review tickets:
			* https://gitlab.torproject.org/groups/tpo/anti-censorship/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&assignee_id=None
		* Sponsor 96 <-- meskio, shell, onyinyang, cohosh
			* https://gitlab.torproject.org/groups/tpo/-/milestones/24
		* Sponsor 150 <-- meskio working on it
			* https://gitlab.torproject.org/groups/tpo/anti-censorship/-/issues/?label_name%5B%5D=Sponsor%20150


== Announcements ==


== Discussion ==

	* Snowflake UDP-like transport ready for a draft review:
		* https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/merge_requests/219
		* since the prior draft, disabled KCP stream mode, and disabled data channel retransmission (the earlier draft had only turned off "ordered" mode, but had not also turned off "reliable" mode): https://lists.torproject.org/pipermail/anti-censorship-team/2023-March/000286.html


will wait for cohosh to be around:
	* manifest v3 deprecation in browsers and snowflake webextension
		* https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake-webext/-/issues/29
		* https://developer.chrome.com/blog/resuming-the-transition-to-mv3/
		* google chrome will stop supporting mv2 June 2024
		* will the snowflake webextension stop working? do we want to do something? or just recommend firefox?

== Actions ==



== Interesting links ==

	* https://opencollective.com/censorship-circumvention/projects/snowflake-daily-operations/updates/2023-november-update
	* https://ooni.org/post/2023-interview-with-ihueze-nwobilor/
	* https://explorer.ooni.org/findings
		* https://ooni.org/post/2023-launch-ooni-censorship-findings-platform/

== Reading group ==
	* We will discuss "NetShuffle: Circumventing Censorship with Shuffle Proxies at the Edge" on December 14
		* https://www.cs-pk.com/papers/3/
		* Questions to ask and goals to have:
			* What aspects of the paper are questionable?
			* Are there immediate actions we can take based on this work?
			* Are there long-term actions we can take based on this work?
			* Is there future work that we want to call out in hopes that others will pick it up?


== Updates ==
Name:
		This week:
			- What you worked on this week.
		Next week:
			- What you are planning to work on next week.
		Help with:
			- Something you need help with.

cecylia (cohosh): 2023-11-09
	Last week:
	    - conjure bridge maintenance
	    - caught a bug in safelog library
	        - https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40306
	    - caught problem with domain front in conjure
	        - https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/conjure/-/issues/38
	This week:
	    - lox tor browser UX integration
	    - lox distributor testing
	    - look into alternative domain fronting providers
	Needs help with:

dcf: 2023-12-14
	Last week:
		- opened issue to upgrade snowflake bridges to 0.4.8.10 https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40311
		- azure CDN bookkeeping https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Snowflake-costs/diff?version_id=b4faa975599a628653d261641590c78a31a2f21f
	Next week:
		- upgrade snowflake bridges to 0.4.8.10 https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40311
		- review draft MR for unreliable data channels https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/merge_requests/219
		- open issue to have snowflake-client log whenever KCPInErrors is nonzero https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40262#note_2886018
			- parent: https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40267
		- open issue to disable /debug endpoint on snowflake broker
	Before EOY 2023:
		- move snowflake-02 to new VM
Help with:

meskio: 2023-12-14
    Last week:
        - grant writing
        - prepare settings anti-listing logic to be reused by HTTP distributor (rdsys#181)
        - setup rdsys staging server (rdsys#170)
    Next week:
        - investigate missing bridges in the assignments (rdsys#190)

Shelikhoo: 2023-12-14
    Last Week:
				- Work on snowflake performance improvement (WIP): https://gitlab.torproject.org/shelikhoo/snowflake/-/tree/dev-speedwithudp?ref_type=heads
				https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/merge_requests/219
				- Merge request reviews
    Next Week/TODO:
           - Write Tor Spec for Armored URL (continue)
				- Work on snowflake performance improvement (WIP): https://gitlab.torproject.org/shelikhoo/snowflake/-/tree/dev-speedwithudp?ref_type=heads
				- Check any blockers for s150 rdsys HTTPS distributor
           - Merge request reviews

onyinyang: 2023-12-14
	Last week(s):
		- continued and nearly finished Telegram bot dev
		- Splintercon
		
	This week:
		- Finish up Telegram bot dev
		- Add Lox wasm stubs needed by applications team
		- Start of Tor browser integration
		- attempt hyper upgrade again


(long term things were discussed at the meeting!): https://pad.riseup.net/p/tor-ac-community-azaleas-room-keep
                    - brainstorming grouping strategies for Lox
buckets (of bridges) and gathering context on how types of bridges are
distributed/use in practice
                         Question: What makes a bridge usable for a
given user, and how can we encode that to best ensure we're getting the
most appropriate resources to people?
                                 1. Are there some obvious grouping
strategies that we can already consider?
                                  e.g., by PT, by bandwidth (lower
bandwidth bridges sacrificed to open-invitation buckets?), by locale (to
be matched with a requesting user's geoip or something?)
                                 2. Does it make sense to group 3
bridges/bucket, so trusted users have access to 3 bridges (and untrusted
users have access to 1)? More? Less?
